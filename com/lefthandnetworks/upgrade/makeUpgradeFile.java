package com.lefthandnetworks.upgrade;
// 
// Decompiled by Procyon v0.5.29
// 

import java.security.MessageDigest;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;

public class makeUpgradeFile
{
    public static void main(final String[] array) {
        System.out.println("makeUpgradeFile: Just entered...\n");
        FileInputStream fileInputStream = null;
        if (array.length != 4) {
            System.out.println("usage: makeUpgradeFile <upgradefile> <name> <description> <version>");
            System.out.println("where <upgradefile> is an upgrade file");
            System.out.println("<name> is the name of the upgrade (no spaces)");
            System.out.println("<description> is a description");
            System.out.println("and <version> is a version string to embed in the file");
            System.out.println("<upgradefile>[.gz].upgrade will be created");
            System.out.println("<upgradefile> is assumed to be a tar file.");
            System.exit(-1);
        }
        try {
            fileInputStream = new FileInputStream(array[0]);
        }
        catch (FileNotFoundException ex) {
            System.out.println("cannot open " + array[0] + " for reading\n");
            System.exit(-1);
        }
        try {
            final byte[] b = new byte[8];
            if (fileInputStream.skip(257L) != 257L) {
                fileInputStream.close();
                System.out.println(array[0] + " must be a tar file");
                System.exit(-1);
            }
            if (fileInputStream.read(b, 0, 8) != 8) {
                fileInputStream.close();
                System.out.println(array[0] + " must be a tar file");
                System.exit(-1);
            }
            if (b[0] != 117 || b[1] != 115 || b[2] != 116 || b[3] != 97 || b[4] != 114 || b[5] != 32 || b[6] != 32 || b[7] != 0) {
                System.out.println(array[0] + " must be a tar file");
                System.exit(-1);
            }
        }
        catch (IOException ex2) {
            System.out.println("failure reading " + array[0] + "\n");
            System.exit(-1);
            System.out.println("makeUpgradeFile: In finally\n");
            try {
                fileInputStream.close();
            }
            catch (IOException obj) {
                System.out.println("makeUpgradeFile: Try failure in finally: " + obj + "\n");
                System.exit(-1);
            }
        }
        finally {
            System.out.println("makeUpgradeFile: In finally\n");
            try {
                fileInputStream.close();
            }
            catch (IOException obj2) {
                System.out.println("makeUpgradeFile: Try failure in finally: " + obj2 + "\n");
                System.exit(-1);
            }
        }
        try {
            final Process exec = Runtime.getRuntime().exec("tar -C /tmp -xf " + array[0] + " ./install");
            exec.waitFor();
            if (exec.exitValue() != 0) {
                System.out.println(exec.exitValue() + array[0] + " must contain executable ./install\n");
                System.exit(-1);
            }
            final File file = new File("/tmp/install");
            if (!file.exists() || !file.isFile()) {
                System.out.println(array[0] + " must contain executable ./install\n");
                file.delete();
                System.exit(-1);
            }
            final String command = "ls -l /tmp/install";
            int n = 0;
            final Process exec2 = Runtime.getRuntime().exec(command);
            final BufferedInputStream bufferedInputStream = new BufferedInputStream(exec2.getInputStream());
            for (int i = 0; i < 10; ++i) {
                n = (char)bufferedInputStream.read();
            }
            if (n != 120) {
                System.out.println(array[0] + " must contain ./install (executable by all)\n");
                file.delete();
                System.exit(-1);
            }
            exec2.waitFor();
            if (exec2.exitValue() != 0) {
                System.out.println(exec2.exitValue() + array[0] + " ls -l /tmp/install failed\n");
                System.exit(-1);
            }
            file.delete();
        }
        catch (Exception x) {
            System.out.println("error checking for ./install\n");
            System.out.println(x);
            System.out.println(array[0] + " must contain executable ./install\n");
            System.exit(-1);
        }
        try {
            System.out.println("Gzipping file " + array[0] + " ...");
            final Process exec3 = Runtime.getRuntime().exec("gzip " + array[0]);
            exec3.waitFor();
            if (exec3.exitValue() != 0) {
                System.out.println("Gzipping " + array[0] + " failed, return code: " + exec3.exitValue());
                System.exit(-1);
            }
        }
        catch (Exception ex3) {
            System.out.println("error gzipping file\n");
            System.exit(-1);
        }
        if (array[1].contains("extended_information")) {
            try {
                System.out.println("Calculating MD5 for " + array[0] + ".gz ...");
                final String md5Value = getMD5Value(array[0] + ".gz", null);
                System.out.println("MD5: <" + md5Value + ">");
                array[1] = replaceMD5(array[1], md5Value);
            }
            catch (Exception obj3) {
                System.out.println("makeUpgradeFile: In try calculating MD5: " + obj3 + "\n");
                System.exit(-1);
            }
        }
        try {
            UpgradeFile.createFile(array[0] + ".gz", array[1], array[2], array[3], array[0] + ".gz.upgrade");
        }
        catch (IOException ex4) {
            System.out.println("error creating upgrade file\n");
            System.exit(-1);
        }
    }
    
    public static String getMD5Value(final String s, final MD5_Check_Progress_Interface md5_Check_Progress_Interface) throws Exception {
        final byte[] checksum = createChecksum(s, md5_Check_Progress_Interface);
        String string = "";
        for (int i = 0; i < checksum.length; ++i) {
            string += Integer.toString((checksum[i] & 0xFF) + 256, 16).substring(1);
        }
        return string;
    }
    
    private static byte[] createChecksum(final String name, final MD5_Check_Progress_Interface md5_Check_Progress_Interface) throws Exception {
        final FileInputStream fileInputStream = new FileInputStream(name);
        final byte[] array = new byte[1048576];
        final MessageDigest instance = MessageDigest.getInstance("MD5");
        long progress = 0L;
        int i;
        do {
            i = fileInputStream.read(array);
            if (i > 0) {
                instance.update(array, 0, i);
                if (md5_Check_Progress_Interface == null) {
                    continue;
                }
                progress += i;
                md5_Check_Progress_Interface.setProgress(progress);
            }
        } while (i != -1);
        fileInputStream.close();
        return instance.digest();
    }
    
    private static String replaceMD5(final String s, final String str) {
        return s.substring(0, s.indexOf("<tarball_md5sum>") + "<tarball_md5sum>".length()) + str + s.substring(s.indexOf("</tarball_md5sum>"));
    }
}
