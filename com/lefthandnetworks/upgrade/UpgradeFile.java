package com.lefthandnetworks.upgrade;
// 
// Decompiled by Procyon v0.5.29
// 

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.Serializable;

public class UpgradeFile implements Serializable
{
    private static final long serialVersionUID = -2777618725922181233L;
    public static final int upgradeBufferSize = 1048576;
    protected boolean appliedOK;
    protected UpgradeDate date;
    protected Long datasize;
    protected UpgradeName name;
    protected UpgradeDescription description;
    protected Signature signature;
    protected Version version;
    
    public UpgradeFile() {
        this.appliedOK = false;
        this.date = null;
        this.signature = null;
        this.version = null;
        this.name = null;
        this.description = null;
        this.datasize = null;
    }
    
    public static void createFile(final String s, final String s2, final String s3, final String s4, final String name) throws FileNotFoundException, IOException {
        final byte[] array = new byte[1048576];
        final MagicNumber obj = new MagicNumber(s4);
        final UpgradeDate obj2 = new UpgradeDate();
        final Signature obj3 = new Signature();
        final UpgradeName obj4 = new UpgradeName(s2);
        final UpgradeDescription obj5 = new UpgradeDescription(s3);
        final Long obj6 = new Long(new File(s).length());
        final BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(name));
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(s));
        obj2.setData("??/??/????");
        obj3.setData(SignatureData.getSignature());
        objectOutputStream.writeObject(obj);
        objectOutputStream.writeObject(new Version("1.0.0"));
        objectOutputStream.writeObject(obj2);
        objectOutputStream.writeObject(obj3);
        objectOutputStream.writeObject(obj4);
        objectOutputStream.writeObject(obj5);
        objectOutputStream.writeObject(obj6);
        objectOutputStream.flush();
        int read;
        while ((read = bufferedInputStream.read(array, 0, array.length)) >= 0) {
            out.write(array, 0, read);
        }
        out.flush();
        out.close();
        objectOutputStream.close();
        bufferedInputStream.close();
    }
    
    public void send(final String s, final InterProcessCom interProcessCom) throws FileNotFoundException, IOException, Exception {
        this.send(s, interProcessCom, null, null, null);
    }
    
    public void send(final String s, final InterProcessCom interProcessCom, final PercentCompleteInterface percentCompleteInterface, final String s2, final UpgradeCanceledInterface upgradeCanceledInterface) throws FileNotFoundException, IOException, Exception {
        if (upgradeCanceledInterface != null && upgradeCanceledInterface.isCanceled()) {
            return;
        }
        long n = 0L;
        final byte[] b = new byte[1048576];
        final FileInputStream fileInputStream = new FileInputStream(s);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        try {
            interProcessCom.send(this.version = (Version)objectInputStream.readObject());
            interProcessCom.send(this.date = (UpgradeDate)objectInputStream.readObject());
            interProcessCom.send(this.signature = (Signature)objectInputStream.readObject());
            this.name = (UpgradeName)objectInputStream.readObject();
            if (s2 != null) {
                this.name = new UpgradeName(s2);
            }
            interProcessCom.send(this.name);
            this.description = (UpgradeDescription)objectInputStream.readObject();
            interProcessCom.send(new UpgradeDescription(s + ": " + this.description.toString()));
            interProcessCom.send(this.datasize = (Long)objectInputStream.readObject());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            objectInputStream.close();
            bufferedInputStream.close();
            throw ex;
        }
        if (upgradeCanceledInterface != null && upgradeCanceledInterface.isCanceled()) {
            objectInputStream.close();
            bufferedInputStream.close();
            fileInputStream.close();
            return;
        }
        long n2 = System.currentTimeMillis();
        if (percentCompleteInterface != null) {
            percentCompleteInterface.updatePercentComplete((long)(n / this.datasize * 100.0));
        }
        while (n < this.datasize) {
            if (System.currentTimeMillis() - n2 > 1000L) {
                n2 = System.currentTimeMillis();
                if (percentCompleteInterface != null) {
                    percentCompleteInterface.updatePercentComplete((long)(n / this.datasize * 100.0));
                }
                if (upgradeCanceledInterface != null && upgradeCanceledInterface.isCanceled()) {
                    objectInputStream.close();
                    bufferedInputStream.close();
                    fileInputStream.close();
                    return;
                }
            }
            final int read = bufferedInputStream.read(b, 0, b.length);
            n += read;
            interProcessCom.send(b, read);
        }
        if (percentCompleteInterface != null) {
            percentCompleteInterface.updatePercentComplete(100L);
            percentCompleteInterface.copyComplete();
        }
        objectInputStream.close();
        bufferedInputStream.close();
        fileInputStream.close();
    }
    
    public void receiveData(final String name, final InterProcessCom interProcessCom) throws IOException, FileNotFoundException, InterProcessComException, Exception {
        final byte[] b = new byte[1048576];
        int i = 0;
        FileOutputStream out = null;
        BufferedOutputStream bufferedOutputStream = null;
        this.readHeaders(interProcessCom);
        final int intValue = (int)(Object)this.datasize;
        if (name != null) {
            out = new FileOutputStream(name);
            bufferedOutputStream = new BufferedOutputStream(out);
        }
        while (i < intValue) {
            final int receive = interProcessCom.receive(b);
            i += receive;
            if (bufferedOutputStream != null) {
                bufferedOutputStream.write(b, 0, receive);
            }
        }
        if (bufferedOutputStream != null) {
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        }
        if (out != null) {
            out.close();
        }
    }
    
    public UpgradeDate receiveDate(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        this.readHeaders(interProcessCom);
        return this.date;
    }
    
    public UpgradeDescription receiveDescription(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        this.readHeaders(interProcessCom);
        return this.description;
    }
    
    public UpgradeDescription receiveDescription(final String s) throws Exception {
        this.readHeaders(s);
        return this.description;
    }
    
    public UpgradeName receiveName(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        this.readHeaders(interProcessCom);
        return this.name;
    }
    
    public Signature receiveSignature(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        this.readHeaders(interProcessCom);
        return this.signature;
    }
    
    public Version receiveVersion(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        this.readHeaders(interProcessCom);
        return this.version;
    }
    
    public Version receiveVersion(final String s) throws Exception {
        this.readHeaders(s);
        return this.version;
    }
    
    public UpgradeName receiveName(final String s) throws Exception {
        this.readHeaders(s);
        return this.name;
    }
    
    protected void readHeaders(final InterProcessCom interProcessCom) throws InterProcessComException, Exception {
        if (this.version == null) {
            this.version = (Version)interProcessCom.receive();
        }
        if (this.date == null) {
            this.date = (UpgradeDate)interProcessCom.receive();
        }
        if (this.signature == null) {
            this.signature = (Signature)interProcessCom.receive();
        }
        if (this.name == null) {
            this.name = (UpgradeName)interProcessCom.receive();
        }
        if (this.description == null) {
            this.description = (UpgradeDescription)interProcessCom.receive();
        }
        if (this.datasize == null) {
            this.datasize = (Long)interProcessCom.receive();
        }
    }
    
    protected void readHeaders(final String name) throws Exception {
        final FileInputStream in = new FileInputStream(name);
        final ObjectInputStream objectInputStream = new ObjectInputStream(in);
        if (this.version == null) {
            this.version = (Version)objectInputStream.readObject();
        }
        if (this.date == null) {
            this.date = (UpgradeDate)objectInputStream.readObject();
        }
        if (this.signature == null) {
            this.signature = (Signature)objectInputStream.readObject();
        }
        if (this.name == null) {
            this.name = (UpgradeName)objectInputStream.readObject();
        }
        if (this.description == null) {
            this.description = (UpgradeDescription)objectInputStream.readObject();
        }
        if (this.datasize == null) {
            this.datasize = (Long)objectInputStream.readObject();
        }
        objectInputStream.close();
        in.close();
    }
    
    public boolean getAppliedOK() {
        return this.appliedOK;
    }
    
    public void setAppliedOK(final boolean appliedOK) {
        this.appliedOK = appliedOK;
    }
    
    public UpgradeDate getUpgradeDate() {
        return this.date;
    }
    
    public Long getDatasize() {
        return this.datasize;
    }
    
    public UpgradeName getUpgradeName() {
        return this.name;
    }
    
    public UpgradeDescription getUpgradeDescription() {
        return this.description;
    }
    
    public Signature getSignature() {
        return this.signature;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public static UpgradeFile read(final String name) throws Exception {
        final FileInputStream in = new FileInputStream(name);
        final ObjectInputStream objectInputStream = new ObjectInputStream(in);
        final UpgradeFile upgradeFile = (UpgradeFile)objectInputStream.readObject();
        objectInputStream.close();
        in.close();
        return upgradeFile;
    }
    
    public void write(final String name) throws Exception {
        final FileOutputStream out = new FileOutputStream(name);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
        objectOutputStream.writeObject(this);
        objectOutputStream.close();
        out.close();
    }
}
