package com.lefthandnetworks.upgrade;
// 
// Decompiled by Procyon v0.5.29
// 

import java.io.Serializable;

public class StreamRecord implements Serializable
{
    private static final long serialVersionUID = -3434554303254718408L;
    protected byte[] data;
    
    StreamRecord() {
    }
    
    StreamRecord(final byte[] data) {
        this.setData(data);
    }
    
    StreamRecord(final String data) {
        this.setData(data);
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public void setData(final byte[] array) {
        System.arraycopy(array, 0, this.data = new byte[array.length], 0, array.length);
    }
    
    public void setData(final String s) {
        this.setData(s.getBytes());
    }
    
    @Override
    public String toString() {
        return new String(this.data);
    }
}
