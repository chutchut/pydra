package com.lefthandnetworks.upgrade;
// 
// Decompiled by Procyon v0.5.29
// 

public class InterProcessComException extends Exception
{
    private static final long serialVersionUID = -7990182645927652675L;
    
    public InterProcessComException() {
    }
    
    public InterProcessComException(final String message) {
        super(message);
    }
}
