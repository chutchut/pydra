package com.lefthandnetworks.upgrade;
// 
// Decompiled by Procyon v0.5.29
// 

import java.net.SocketAddress;
import java.io.ObjectOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

public class InterProcessCom
{
    private Socket socketComm;
    private String socketIP;
    private int port;
    private ObjectInputStream objectReader;
    private BufferedInputStream bufferedReader;
    private BufferedOutputStream bufferedWriter;
    private ObjectOutputStream objectWriter;
    
    public InterProcessCom() {
        this.socketComm = null;
        this.objectReader = null;
        this.bufferedReader = null;
        this.bufferedWriter = null;
        this.objectWriter = null;
    }
    
    public SocketAddress getRemoteAddress() {
        if (this.socketComm != null) {
            return this.socketComm.getRemoteSocketAddress();
        }
        return null;
    }
    
    public InterProcessCom(final String socketIP, final int port) {
        this.socketComm = null;
        this.objectReader = null;
        this.bufferedReader = null;
        this.bufferedWriter = null;
        this.objectWriter = null;
        this.socketIP = socketIP;
        this.port = port;
    }
    
    public void setSocketIP(final String socketIP) {
        this.socketIP = socketIP;
    }
    
    public void setSocket(final Socket socketComm) {
        this.socketComm = socketComm;
    }
    
    public void setPort(final int port) {
        this.port = port;
    }
    
    public void openSocket() throws InterProcessComException {
        try {
            this.closeSocket();
            this.socketComm = new Socket(this.socketIP, this.port);
        }
        catch (Exception ex) {
            throw new InterProcessComException("Exception thrown opening socket.");
        }
    }
    
    public void closeSocket() throws InterProcessComException {
        try {
            if (this.socketComm != null) {
                this.socketComm.close();
                this.socketComm = null;
            }
        }
        catch (Exception ex) {
            throw new InterProcessComException("Exception thrown closing socket.");
        }
    }
    
    public void send(final Object obj) throws Exception {
        try {
            if (this.objectWriter == null) {
                this.objectWriter = new ObjectOutputStream(this.socketComm.getOutputStream());
            }
            this.objectWriter.reset();
            this.objectWriter.writeObject(obj);
            this.objectWriter.flush();
        }
        catch (Exception ex) {
            if (obj == null) {
                System.out.println("Tried to send a null object to the socket");
            }
            else {
                System.out.println("Unable to send object of type: " + obj.getClass().getName() + " to scoket ");
            }
            throw ex;
        }
    }
    
    public void send(final byte[] b, final int len) throws Exception {
        try {
            if (this.bufferedWriter == null) {
                this.bufferedWriter = new BufferedOutputStream(this.socketComm.getOutputStream());
            }
            this.bufferedWriter.write(b, 0, len);
            this.bufferedWriter.flush();
        }
        catch (Exception ex) {
            throw ex;
        }
    }
    
    private void setSoTimeout(final int soTimeout) {
        if (this.socketComm == null) {
            return;
        }
        try {
            this.socketComm.setSoTimeout(soTimeout);
        }
        catch (Exception ex) {}
    }
    
    private int getSoTimeout() {
        if (this.socketComm == null) {
            return 0;
        }
        try {
            return this.socketComm.getSoTimeout();
        }
        catch (Exception ex) {
            return 0;
        }
    }
    
    public Object receive(final int soTimeout) throws Exception {
        final int soTimeout2 = this.getSoTimeout();
        try {
            this.setSoTimeout(soTimeout);
            final Object receive = this.receive();
            this.setSoTimeout(soTimeout2);
            return receive;
        }
        catch (Exception ex) {
            this.setSoTimeout(soTimeout2);
            throw ex;
        }
    }
    
    public Object receive() throws Exception {
        Object object;
        try {
            if (this.objectReader == null) {
                this.objectReader = new ObjectInputStream(this.socketComm.getInputStream());
            }
            object = this.objectReader.readObject();
        }
        catch (Exception ex) {
            throw ex;
        }
        return object;
    }
    
    public int receive(final byte[] b) throws Exception {
        final int length = b.length;
        int read;
        try {
            if (this.bufferedReader == null) {
                this.bufferedReader = new BufferedInputStream(this.socketComm.getInputStream());
            }
            read = this.bufferedReader.read(b, 0, length);
        }
        catch (Exception ex) {
            throw ex;
        }
        return read;
    }
}
