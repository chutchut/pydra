// 
// Decompiled by Procyon v0.5.29
// 

package com.lefthandnetworks.upgrade;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;

class extractUpgradeFile
{
    public static void main(final String[] array) {
        int n = 0;
        final byte[] array2 = new byte[1048576];
        if (array.length < 2) {
            System.err.println("usage: extractUpgradeFile <infile> <outfile>");
            System.exit(0);
        }
        try {
            final FileInputStream fileInputStream = new FileInputStream(array[0]);
            final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            final BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(array[1]));
            objectInputStream.readObject();
            objectInputStream.readObject();
            objectInputStream.readObject();
            objectInputStream.readObject();
            objectInputStream.readObject();
            final Long obj = (Long)objectInputStream.readObject();
            System.out.println("DATA SIZE: " + obj);
            while (n < obj) {
                final int read = bufferedInputStream.read(array2, 0, array2.length);
                n += read;
                bufferedOutputStream.write(array2, 0, read);
            }
            bufferedOutputStream.flush();
            System.out.println("Finished extracting .gz file!");
            bufferedInputStream.close();
            bufferedOutputStream.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
